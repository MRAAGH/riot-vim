" ADD THE CONTENT OF THIS VILE TO YOUR ~/.vimrc

" vim riot
autocmd BufEnter .vimriot set filetype=vimriot
autocmd FileType vimriot nnoremap <silent> <CR> :.w !xclip -selection clipboard && riot-vim &<CR><CR>
autocmd FileType vimriot vnoremap <silent> <CR> :w !xclip -selection clipboard && riot-vim &<CR><CR>
autocmd BufEnter .vimriotselect set filetype=vimriotselect
autocmd FileType vimriotselect nnoremap <silent> <CR> :.w !xclip -selection clipboard && riot-vim -s &<CR><CR>
