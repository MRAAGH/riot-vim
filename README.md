# riot-vim

For those who love Riot for its stability and features, but are stuck with its simple text editor!

![vimriot2.gif](https://img.ourl.ca/vimriot2.gif)

# Installation

The only supported Matrix client is Riot Electron app running on on GNU/Linux.

Install the following packages:

```
sudo apt-get install xdotool xclip
```

Append the content of the `.vimrc` file in this repo to your own `.vimrc` file.

Download the file `riot-vim` and put it in some directory that's part of your `$PATH`.

# Usage

Run the command:
`vim ~/.vimriot ~/.vimriotselect -O`
(pro tip: run this automatically when starting Riot)

Type something in the left window (`.vimriot`), then press Enter in normal mode.
Your message will be copied into Riot and sent.

Highlight a bunch of lines and press Enter. The selection will be sent as a single message.

Type the name of a room in the right window (`.vimriotselect`), then press Enter in normal mode.
That's how you jump to a room.

If anything is not working properly, open the file `riot-vim` and slightly increase the delays.

